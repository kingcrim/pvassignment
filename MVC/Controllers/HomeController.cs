﻿
using Domain;
using Microsoft.AspNetCore.Mvc;
using MVC.ViewModels;
using System;

namespace MVC.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            var model = new MultiplesDisplayViewModel();

            return View(model);
        }
        public IActionResult Error()
        {
            return View();
        }

        public IActionResult GetMultiples(MultiplesInputViewModel input)
        {
            var output = new MultiplesOutputViewModel();
            output.EventType = input.EventType;

            if (input.EventType == "Register")
            {
                var ret = new RegisterEventType();

                for(int i = 1; i < 101; i++)
                {
                    var tup = new Tuple<int, string>(i, ret.CheckAgainstNumber(i));
                    output.Multiples.Add(tup);
                }
            }
            if (input.EventType == "Diagnose")
            {
                var det = new DiagnoseEventType();
                for (int i = 1; i < 101; i++)
                {
                    var tup = new Tuple<int, string>(i, det.CheckAgainstNumber(i));
                    output.Multiples.Add(tup);
                }
            }

            return View("Multiples", output);
        }
    }
}
