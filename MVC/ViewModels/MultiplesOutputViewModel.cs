﻿

using System;
using System.Collections.Generic;

namespace MVC.ViewModels
{
    public class MultiplesOutputViewModel
    {
        public List<Tuple<int, string>> Multiples { get; set; }

        public string EventType { get; set; }

        public MultiplesOutputViewModel()
        {
            Multiples = new List<Tuple<int, string>>();
        }
    }
}
