﻿
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Collections.Generic;


namespace MVC.ViewModels
{
    public class MultiplesDisplayViewModel
    {
        public SelectList EventTypes { get; set; }

        public MultiplesDisplayViewModel()
        {
            var items = new List<string>();
            items.Add("Register");
            items.Add("Diagnose");

            EventTypes = new SelectList(items);
        }

    }
}
