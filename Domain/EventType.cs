﻿
using System;
using System.Collections.Generic;
using System.Linq;

namespace Domain
{
    interface IEventType
    {
        List<RuleOutput> Rules { get; }

        string Name { get; }

        string CheckAgainstNumber(int number);
    }

    public abstract class EventType : IEventType
    {
        public virtual List<RuleOutput> Rules => throw new NotImplementedException();

        public virtual string Name => throw new NotImplementedException();

        public virtual string CheckAgainstNumber(int number)
        {
            List<RuleOutput> rulesThatMatch = new List<RuleOutput>();

            foreach (var ro in Rules)
            {
                if (ro.CheckAgainstNumber(number) != number.ToString())
                {
                    rulesThatMatch.Add(ro);
                }
            }


            if (rulesThatMatch.Count == 0)
                return number.ToString();

            return rulesThatMatch.OrderByDescending(x => x.Priority).First().OutputWhenMultiplesSuccessfullyChecked;
        }
    }


    public class RegisterEventType : EventType
    {

        public override string Name
        {
            get {
                return "Register";
            }
        }

        public override List<RuleOutput> Rules
        {
            get
            {
                var values = new List<RuleOutput>();

                var rule1 = new RuleOutput { MultiplesToCheck = new List<int>(new int[] { 3 }), OutputWhenMultiplesSuccessfullyChecked = "Register", Priority = 1 };

                values.Add(rule1);

                var rule2 = new RuleOutput { MultiplesToCheck = new List<int>(new int[] { 5 }), OutputWhenMultiplesSuccessfullyChecked = "Patient", Priority = 1 };

                values.Add(rule2);

                var rule3 = new RuleOutput { MultiplesToCheck = new List<int>(new int[] { 3,5 }), OutputWhenMultiplesSuccessfullyChecked = "Register Patient", Priority = 10 };

                values.Add(rule3);



                return values;
            }
        }

    }

    public class DiagnoseEventType : EventType
    {


        public override string Name
        {
            get
            {
                return "Diagnose";
            }
        }

        public override List<RuleOutput> Rules
        {
            get
            {
                var values = new List<RuleOutput>();

                var rule1 = new RuleOutput { MultiplesToCheck = new List<int>(new int[] { 2 }), OutputWhenMultiplesSuccessfullyChecked = "Diagnose", Priority = 1 };

                values.Add(rule1);

                var rule2 = new RuleOutput { MultiplesToCheck = new List<int>(new int[] { 7 }), OutputWhenMultiplesSuccessfullyChecked = "Patient", Priority = 1 };

                values.Add(rule2);

                var rule3 = new RuleOutput { MultiplesToCheck = new List<int>(new int[] { 2, 7 }), OutputWhenMultiplesSuccessfullyChecked = "Diagnose Patient", Priority = 10 };

                values.Add(rule3);



                return values;
            }
        }
    }
}
