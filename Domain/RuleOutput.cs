﻿

using Extensions;
using System.Collections.Generic;

namespace Domain
{
    public class RuleOutput
    {
        public List<int> MultiplesToCheck { get; set; } 
        public string OutputWhenMultiplesSuccessfullyChecked { get; set; }

        public int Priority { get; set; }

        public string CheckAgainstNumber(int number)
        {
            foreach(var i in MultiplesToCheck)
            {
                if (number.IsNotAMultipleOf(i))
                    return number.ToString();
            }

            return OutputWhenMultiplesSuccessfullyChecked;
        }
    }
}
