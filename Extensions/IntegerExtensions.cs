﻿
namespace Extensions
{
    public static class IntegerExtensions
    {
        public static bool IsAMultipleOf(this int x, int multiple)
        {
            return (x % multiple) == 0;
        }

        public static bool IsNotAMultipleOf(this int x, int multiple)
        {
            return (x % multiple) != 0;
        }
    }
}
