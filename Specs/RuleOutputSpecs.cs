﻿

using Domain;
using System.Collections.Generic;
using Xunit;

namespace Specs
{
    public class RuleOutputSpecs
    {
        #region Register Event Rule Output Specs
        [Fact]
        public void a_rule_output_of_multiple_three_returning_register_returns_register_when_checked_against_three()
        {
            var ro = new RuleOutput { MultiplesToCheck = new List<int>(new int[] { 3 }), OutputWhenMultiplesSuccessfullyChecked = "Register" };

            var output = ro.CheckAgainstNumber(3);

            Assert.True(output == "Register");
        }

        [Fact]
        public void a_rule_output_of_multiple_three_returning_register_returns_four_when_checked_against_four()
        {
            var ro = new RuleOutput { MultiplesToCheck = new List<int>(new int[] { 3 }), OutputWhenMultiplesSuccessfullyChecked = "Register" };

            var output = ro.CheckAgainstNumber(4);

            Assert.True(output == 4.ToString());
        }

        [Fact]
        public void a_rule_output_of_multiple_five_returning_patient_returns_patient_when_checked_against_five()
        {
            var ro = new RuleOutput { MultiplesToCheck = new List<int>(new int[] { 5 }), OutputWhenMultiplesSuccessfullyChecked = "Patient" };

            var output = ro.CheckAgainstNumber(5);

            Assert.True(output == "Patient");
        }

        [Fact]
        public void a_rule_output_of_multiple_five_returning_patient_returns_four_when_checked_against_four()
        {
            var ro = new RuleOutput { MultiplesToCheck = new List<int>(new int[] { 5 }), OutputWhenMultiplesSuccessfullyChecked = "Patient" };

            var output = ro.CheckAgainstNumber(4);

            Assert.True(output == 4.ToString());
        }

        [Fact]
        public void a_rule_output_of_multiples_three_and_five_returning_register_patient_returns_register_patient_when_checked_against_fifteen()
        {
            var ro = new RuleOutput { MultiplesToCheck = new List<int>(new int[] { 3,5 }), OutputWhenMultiplesSuccessfullyChecked = "Register Patient" };

            var output = ro.CheckAgainstNumber(15);

            Assert.True(output == "Register Patient");
        }

        [Fact]
        public void a_rule_output_of_multiples_three_and_five_returning_register_patient_returns_fourteen_when_checked_against_fourteen()
        {
            var ro = new RuleOutput { MultiplesToCheck = new List<int>(new int[] { 3, 5 }), OutputWhenMultiplesSuccessfullyChecked = "Register Patient" };

            var output = ro.CheckAgainstNumber(14);

            Assert.True(output == 14.ToString());
        }
        #endregion

        #region Diagnose Event Rule Output Specs
        [Fact]
        public void a_rule_output_of_multiple_two_returning_diagnose_returns_diagnose_when_checked_against_two()
        {
            var ro = new RuleOutput { MultiplesToCheck = new List<int>(new int[] { 2 }), OutputWhenMultiplesSuccessfullyChecked = "Diagnose" };

            var output = ro.CheckAgainstNumber(2);

            Assert.True(output == "Diagnose");
        }

        [Fact]
        public void a_rule_output_of_multiple_two_returning_diagnose_returns_three_when_checked_against_three()
        {
            var ro = new RuleOutput { MultiplesToCheck = new List<int>(new int[] { 2 }), OutputWhenMultiplesSuccessfullyChecked = "Diagnose" };

            var output = ro.CheckAgainstNumber(3);

            Assert.True(output == 3.ToString());
        }

        [Fact]
        public void a_rule_output_of_multiple_seven_returning_patient_returns_patient_when_checked_against_seven()
        {
            var ro = new RuleOutput { MultiplesToCheck = new List<int>(new int[] { 7 }), OutputWhenMultiplesSuccessfullyChecked = "Patient" };

            var output = ro.CheckAgainstNumber(7);

            Assert.True(output == "Patient");
        }

        [Fact]
        public void a_rule_output_of_multiple_seven_returning_patient_returns_four_when_checked_against_four()
        {
            var ro = new RuleOutput { MultiplesToCheck = new List<int>(new int[] { 7 }), OutputWhenMultiplesSuccessfullyChecked = "Patient" };

            var output = ro.CheckAgainstNumber(4);

            Assert.True(output == 4.ToString());
        }

        [Fact]
        public void a_rule_output_of_multiples_two_and_seven_returning_diagnose_patient_returns_diagnose_patient_when_checked_against_fourteen()
        {
            var ro = new RuleOutput { MultiplesToCheck = new List<int>(new int[] { 2, 7 }), OutputWhenMultiplesSuccessfullyChecked = "Diagnose Patient" };

            var output = ro.CheckAgainstNumber(14);

            Assert.True(output == "Diagnose Patient");
        }

        [Fact]
        public void a_rule_output_of_multiples_two_and_seven_returning_diagnose_patient_returns_fifteen_when_checked_against_fifteen()
        {
            var ro = new RuleOutput { MultiplesToCheck = new List<int>(new int[] { 2, 7 }), OutputWhenMultiplesSuccessfullyChecked = "Diagnose Patient" };

            var output = ro.CheckAgainstNumber(15);

            Assert.True(output == 15.ToString());
        }
        #endregion
    }
}
