﻿using Extensions;

using Xunit;

namespace Specs
{
    public class MultiplesSpecs
    {
        [Fact]
        public void three_is_a_multiple_of_three()
        {
            Assert.True(3.IsAMultipleOf(3));
        }

        [Theory]
        [InlineData(2, 2)]
        [InlineData(3, 3)]
        [InlineData(5, 5)]
        [InlineData(7, 7)]
        public void numbers_are_multiples_of_themseleves(int number, int itself)
        {
            Assert.True(number.IsAMultipleOf(itself));
        }

        [Fact]
        public void two_is_not_a_multiple_of_three()
        {
            Assert.True(2.IsNotAMultipleOf(3));
        }

        [Theory]
        [InlineData(2, 3)]
        [InlineData(3, 5)]
        [InlineData(5, 7)]
        [InlineData(7, 2)]
        public void non_multiples_are_not_multiples(int number, int other)
        {
            Assert.True(number.IsNotAMultipleOf(other));
        }
    }
}
