﻿

using Domain;
using Xunit;

namespace Specs
{
    public class DiagnoseEventTypeSpecs
    {
        [Fact]
        public void when_checked_against_two_returns_diagnose()
        {
            var det = new DiagnoseEventType();

            var output = det.CheckAgainstNumber(2);

            Assert.True(output == "Diagnose");
        }

        [Fact]
        public void when_checked_against_seven_returns_patient()
        {

            var det = new DiagnoseEventType();

            var output = det.CheckAgainstNumber(7);

            Assert.True(output == "Patient");
        }

        [Fact]
        public void when_checked_against_fourteen_returns_diagnose_patient()
        {

            var det = new DiagnoseEventType();

            var output = det.CheckAgainstNumber(14);

            Assert.True(output == "Diagnose Patient");
        }

        [Fact]
        public void when_checked_against_fifteen_returns_fifteen()
        {

            var det = new DiagnoseEventType();

            var output = det.CheckAgainstNumber(15);

            Assert.True(output == 15.ToString());
        }
    }
}
