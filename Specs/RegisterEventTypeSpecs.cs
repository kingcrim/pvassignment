﻿

using Domain;
using Xunit;

namespace Specs
{
    public class RegisterEventTypeSpecs
    {
        [Fact]
        public void when_checked_against_three_returns_register()
        {
            var ret = new RegisterEventType();

            var output = ret.CheckAgainstNumber(3);

            Assert.True(output == "Register");
        }

        [Fact]
        public void when_checked_against_five_returns_patient()
        {
            var ret = new RegisterEventType();

            var output = ret.CheckAgainstNumber(5);

            Assert.True(output == "Patient");
        }

        [Fact]
        public void when_checked_against_fifteen_returns_register_patient()
        {
            var ret = new RegisterEventType();

            var output = ret.CheckAgainstNumber(15);

            Assert.True(output == "Register Patient");
        }

        [Fact]
        public void when_checked_against_seventeen_returns_seventeen()
        {
            var ret = new RegisterEventType();

            var output = ret.CheckAgainstNumber(17);

            Assert.True(output == 17.ToString());
        }
    }
}
